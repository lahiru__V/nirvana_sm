const customerProfile = require('../models/customerProfile');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();


router.post('/register', async (req, res) => {
    try {
        const { firstName, lastName, email, password, userType } = req.body;
        const hashedPass = await bcrypt.hash(password, 10);

        const existingUser = await customerProfile.findOne({ email });
        if (existingUser) {
            return res.json({
                message: 'Email is Already Used'
            })
        }

        const cProfile = new customerProfile({
            firstName,
            lastName,
            email,
            password: hashedPass,
            userType,
        });

        await cProfile.save();
        return res.json({ message: 'Added' });

    } catch (error) {
        console.log(error);
        res.status(500).send('Error registering user');
    }
});


router.post('/login', async (req, res) => {

    const { email, password } = req.body;

    try {
        const user = await customerProfile.findOne({ email });

        if (!user) {
            return res.status(400).json({ message: false });
        }

        const isPasswordMatch = await bcrypt.compare(password, user.password);

        if (!isPasswordMatch) {
            return res.status(400).json({ message: false });
        }

        const userTypeSearch = await customerProfile.find({ email, status: 'Approved' });

        if (!userTypeSearch) {
            return res.status(400).json({ message: false });
        }

        return res.status(200).json({ message: true });

    } catch (error) {
        console.log(error);
        res.status(500).send('Error logging in');
    }
});



module.exports = router;