const router = require('express').Router();
let payment_Schema = require('../models/payment');

router.route('/addpayment').post((req, res) => {        
        const { doctorName, patientName, email, phoneNumber, date, note, price } = req.body;
        const payment = new payment_Schema({ doctorName, patientName, email, phoneNumber, date, note, price });
        payment.save()
                .then(() => res.json('Payment Add!'))
                .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;