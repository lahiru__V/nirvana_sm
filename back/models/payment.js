const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const payment = new Schema({

    doctorName: {
        type: String,
        required: true
    },
    patientName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    note: {
        type: String,
        required: true
    }, 
    price: {
        type: Number,
        required: true
    },
}, {
    timestamps: true
});
const payment_Schema = mongoose.model('payment', payment);
module.exports = payment_Schema;

