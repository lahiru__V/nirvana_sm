import { BrowserRouter as Router, Route } from 'react-router-dom'
import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import Index from './components/index';
import UserLogin from './components/user.login';
import UserRegistration from './components/user.registration';
import About from './components/user.About';
import PaymentForm from './components/userAdmin/payment';
import Doctors from './components/userAdmin/doctors';
import Medicine from './components/userAdmin/medicine';
import Booking from './components/userAdmin/booking';
import UserAdminDashboard from './components/userAdmin/userAdminDashboard';


function App() {
  return (
    <Router>
      <div>
        <Route exact path="/" >
          <Index />
        </Route>
        <Route path="/PaymentForm" exact component={PaymentForm} />
        <Route path="/Medicine" exact component={Medicine} />
        <Route path="/Doctors" exact component={Doctors} />        
        <Route path="/UserAdminDashboard" exact component={UserAdminDashboard} />
        <Route path="/UserLogin" exact component={UserLogin} />
        <Route path="/UserRegistration" exact component={UserRegistration} />
        <Route path="/About" exact component={About} />        
        <Route path="/Booking" exact component={Booking} />      
      </div>
    </Router>
  );
}

export default App;
