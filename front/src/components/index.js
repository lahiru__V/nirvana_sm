import React, { useState, useEffect } from 'react';
import {
    MDBCardImage,
    MDBRow, MDBCol
} from 'mdb-react-ui-kit';
import axios from 'axios';
import Swal from 'sweetalert2';
import Navbar from './main_parts/navbar.js';
import Footer from './main_parts/footer.js';

function Home() {

    return (
        <div>
            <div className="pt-1 pb-1" style={{ backgroundColor: '#F4F4F4' }}>
                <center>
                    <small style={{ fontSize: '14px', letterSpacing: '2px' }} className="text-muted text-capitalize">Nirvana Medical Service Hub</small>
                </center>
            </div>
            <Navbar />

            <header class="py-5" style={{ backgroundImage: "url('https://images.unsplash.com/photo-1495461199391-8c39ab674295?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8YXl1cnZlZGF8ZW58MHx8MHx8fDA%3D')", backgroundSize: "cover", backgroundPosition: "center" }}>
                <div class="container px-5">
                    <div class="row gx-5 align-items-center justify-content-center">
                        <div class="col-lg-8 col-xl-7 col-xxl-6">
                            <div className='row'>
                                <div className='col'>
                                    <div class="my-5 text-center text-xl-start">
                                        <h1 class="display-5 fw-bolder text-dark mb-2 text-uppercase">
                                            Nirvana  Healthcare<br />
                                            <span
                                                class="txt-rotate text-warning"
                                                data-period="2000"
                                                data-rotate='[ "Oiling", "Massages", "Natural Medicines"]'></span>
                                        </h1>
                                        <p class="text-white" style={{ fontSize: '20px', fontWeight: 'bold', }}>
                                            The main objective is to contribute to the quality development of the Ayurveda by providing maximum service in accordance with professional ethics
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section className="container mt-5 pt-5 pb-5 mb-5">
                <hr />
                <div className="container">
                    <MDBRow className="mt-4">
                        <MDBCol >
                            <div class="my-5 text-center text-xl-start">
                                <h2 class=" fw-bolder text-black mb-2 text-uppercase">
                                    Explaning ABout Services
                                </h2>
                                <p class="text-black" style={{ fontSize: '18px', fontWeight: 'bold', }}>

                                    In Ayurveda, the ancient Indian system of medicine, the concept of balance and harmony is central to health and well-being. Your website can be likened to the body, with its various elements representing the doshas (Vata, Pitta, and Kapha), which need to be in equilibrium for optimal functioning. Just as Ayurveda seeks to restore balance through personalized treatments, your website should aim to provide tailored solutions or information to meet the unique needs of its visitors. By understanding their requirements and addressing them effectively, your website can promote harmony and fulfillment, much like Ayurvedic principles seek to do for the body and mind.
                                </p>
                            </div>
                        </MDBCol>
                        <MDBCol >
                            <br />
                            <br />
                            <div >
                                <MDBCardImage src='https://images.unsplash.com/photo-1515377905703-c4788e51af15?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D' alt='...' height="
                                400" width="650" />
                            </div>

                        </MDBCol>
                    </MDBRow>
                </div>
                <hr />
            </section>
            <section className="container" style={{ marginTop: '3%', marginBottom: '8%' }}>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card border-0 shadow-0">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.5582270597614!2d79.85892711477123!3d6.910228620021601!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae2594714a4f1e9%3A0x4eacc510183d83b6!2sUnion%20Place%2C%20Colombo!5e0!3m2!1sen!2slk!4v1646737311517!5m2!1sen!2slk" width="100%" height="500" style={{ border: '0' }} allowfullscreen="" loading="lazy"></iframe>
                            <p class="text-muted pt-3 text-left">VaxuallStreet, UnionPlace, Colombo02</p>
                        </div>
                    </div>
                </div>
            </section>
            <br />
            <Footer />
        </div>
    )
};

export default Home;