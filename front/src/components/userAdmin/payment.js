import React, { useState } from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdb-react-ui-kit';
import { reactLocalStorage } from 'reactjs-localstorage';
import axios from 'axios';
import Swal from 'sweetalert2';

const PaymentForm = () => {
  const [formData, setFormData] = useState({
    cardNumber: '',
    cardHolder: '',
    expirationDate: '',
    cvv: ''
  });
  const obj_Pr = reactLocalStorage.getObject('obj_Booking');

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    var obj_Booking = reactLocalStorage.getObject('obj_Booking');
    if (validateForm()) {
      try {
        const response = await axios.post(global.APIUrl + "/payment/addpayment", obj_Booking);
        Swal.fire({
          title: "Success!",
          text: "Payment Succeeded",
          icon: "success",
          confirmButtonText: "OK",
          type: "success",
        });
        reactLocalStorage.setObject("obj_Booking", {});
        setTimeout(() => {
          window.location.href = "/";
        }, 3000);

      } catch (error) {
        console.log(error.message);
        Swal.fire({
          title: "Error!",
          text: "Payment Fail",
          icon: 'error',
          confirmButtonText: "OK",
          type: "error"
        })
        reactLocalStorage.setObject("obj_Booking", {});
        setTimeout(() => {
          window.location.href = "/Booking";
        }, 3000);
      }
    } else {
      Swal.fire({
        title: "Error!",
        text: "Form is invalid. Please check your inputs.",
        icon: 'error',
        confirmButtonText: "OK",
        type: "error"
      })
    }
  };

  const validateForm = () => {

    return (
      formData.cardNumber.length === 16 &&
      formData.cvv.length === 3 &&
      formData.expirationDate.match(/^\d{2}\/\d{2}$/)
    );
  };

  return (
    <MDBContainer className="mt-8" style={{ paddingTop: "200px" }}>
      <MDBRow className="justify-content-center">
        <MDBCol md="6">
          <h2 className="text-center mb-4">Pay with Card</h2>
          <div className="d-flex justify-content-center mb-4">
            <img style={{ width: "100px", height: "100px" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTROri_1VNKbryL7c0KcJO4g3K7-fITpdozNmdpe1byXw&s" alt="Visa" className="mr-2" />
            <img style={{ width: "100px", height: "100px" }} src="https://imageio.forbes.com/blogs-images/steveolenski/files/2016/07/Mastercard_new_logo-1200x865.jpg?height=512&width=711&fit=bounds" alt="Mastercard" className="mr-2" />
            <img style={{ width: "100px", height: "100px" }} src="https://image.spreadshirtmedia.com/image-server/v1/mp/products/T1459A839PA3861PT28D1041652815W10000H2856/views/1,width=800,height=800,appearanceId=839,backgroundColor=F2F2F2/american-express-amex-logo-sticker.jpg" alt="American Express" />
          </div>
          <h4 className="text-center mb-4">Need to Pay : {obj_Pr.price} LKR</h4>
          <form onSubmit={handleSubmit} className="border p-4 rounded">
            <MDBInput
              label="Card Number"
              type="text"
              name="cardNumber"
              value={formData.cardNumber}
              onChange={handleInputChange}
              className="mb-4"
              pattern="[0-9]{16}"
              maxLength={16}
              required
            />
            <MDBInput
              label="Card Holder"
              type="text"
              name="cardHolder"
              value={formData.cardHolder}
              onChange={handleInputChange}
              className="mb-4"
              required
            />
            <MDBRow>
              <MDBCol md="6">
                <MDBInput
                  label="Expiration Date (MM/YY)"
                  type="text"
                  name="expirationDate"
                  value={formData.expirationDate}
                  onChange={handleInputChange}
                  className="mb-4"
                  pattern="^\d{2}\/\d{2}$"
                  maxLength={5}
                  required
                />
              </MDBCol>
              <MDBCol md="6">
                <MDBInput
                  label="CVV"
                  type="text"
                  name="cvv"
                  value={formData.cvv}
                  onChange={handleInputChange}
                  className="mb-4"
                  pattern="[0-9]{3}"
                  maxLength={3}
                  required
                />
              </MDBCol>
            </MDBRow>
            <MDBBtn color="primary" type="submit" className="w-100 mt-3">
              Pay Now
            </MDBBtn>
          </form>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default PaymentForm;
