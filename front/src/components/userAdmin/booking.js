import React, { useState } from 'react';
import Navbar from '../main_parts/navbar.user.log.js';
import Footer from '../main_parts/footer.js';
import { reactLocalStorage } from 'reactjs-localstorage';

function Booking() {
    const [patientName, setPatientName] = useState('');
    const [email, setEmail] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [date, setDate] = useState('');
    const [note, setNote] = useState('');
    const doctorName = sessionStorage.getItem('doctor_name');

    const handleSubmit = (e) => {
        e.preventDefault();        
        var price = 2000;
        var obj = { doctorName, patientName, email, phoneNumber, date, note, price};
        reactLocalStorage.setObject("obj_Booking", obj);  
        window.location.href = "/PaymentForm";      
    };

    return (
        <div>
            <Navbar />
            <div className='container mt-5'>
                <h2 className='mb-4'>Book Appointment with {doctorName}</h2>             
                    <form onSubmit={handleSubmit}>
                        <div className='mb-3'>
                            <label htmlFor='patientName' className='form-label'>Patient Name</label>
                            <input type='text' className='form-control' id='patientName' value={patientName} onChange={(e) => setPatientName(e.target.value)} required />
                        </div>
                        <div className='mb-3'>
                            <label htmlFor='email' className='form-label'>Email</label>
                            <input type='email' className='form-control' id='email' value={email} onChange={(e) => setEmail(e.target.value)} required />
                        </div>
                        <div className='mb-3'>
                            <label htmlFor='phoneNumber' className='form-label'>Phone Number</label>
                            <input type='tel' className='form-control' id='phoneNumber' value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} required />
                        </div>
                        <div className='mb-3'>
                            <label htmlFor='date' className='form-label'>Date</label>
                            <input type='date' className='form-control' id='date' value={date} onChange={(e) => setDate(e.target.value)} required />
                        </div>
                        <div className='mb-3'>
                            <label htmlFor='note' className='form-label'>Note</label>
                            <textarea className='form-control' id='note' rows='3' value={note} onChange={(e) => setNote(e.target.value)}></textarea>
                        </div>
                        <button
                            type='submit'
                            style={{
                                backgroundColor: '#007bff', /* Blue */
                                border: 'none',
                                color: 'white',
                                padding: '10px 20px',
                                textAlign: 'center',
                                textDecoration: 'none',
                                display: 'inline-block',
                                fontSize: '16px',
                                cursor: 'pointer',
                                borderRadius: '5px'
                            }}
                        >
                            Book Now
                        </button>
                    </form>             
            </div>
            <br/>
            <br/>
            <Footer />
        </div>
    );
}

export default Booking;
