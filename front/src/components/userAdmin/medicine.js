import React, { useState } from 'react';
import Navbar from '../main_parts/navbar.user.log.js';
import Footer from '../main_parts/footer.js';
import { MDBModal, MDBModalBody, MDBModalHeader, MDBBtn, MDBCard, MDBCardBody, MDBCardTitle, MDBCardText } from 'mdb-react-ui-kit';

function Medicine() {
    const [modalOpen, setModalOpen] = useState(false);
    const [selectedMedicine, setSelectedMedicine] = useState(null);

    const medicines = [
        { name: 'Diabates', image: 'https://res.cloudinary.com/digipumwy/image/upload/v1709719917/m59uudim8p9j13ir1mli.png', desc: 'In Ayurveda, the ancient Indian system of medicine, the concept of balance and harmony is central to health and well-being. Your website can be likened to the body, with its various elements representing the doshas (Vata, Pitta, and Kapha), which need to be in equilibrium for optimal functioning. Just as Ayurveda seeks to restore balance through personalized treatments, your website should aim to provide tailored solutions or information to meet the unique needs of its visitors. By understanding their requirements and addressing them effectively, your website can promote harmony and fulfillment, much like Ayurvedic principles seek to do for the body and mind. 1' },
        { name: 'Dandruff', image: 'https://res.cloudinary.com/digipumwy/image/upload/v1709719916/ry6cnbvjexaflzpltyi6.png', desc: 'Description 2' },
        { name: 'Snake Venom', image: 'https://res.cloudinary.com/digipumwy/image/upload/v1709719915/ahrn6jasqorsieuxpyak.png', desc: 'Description 3' },
        { name: 'Diabates', image: 'https://res.cloudinary.com/digipumwy/image/upload/v1709719917/m59uudim8p9j13ir1mli.png', desc: 'In Ayurveda, the ancient Indian system of medicine, the concept of balance and harmony is central to health and well-being. Your website can be likened to the body, with its various elements representing the doshas (Vata, Pitta, and Kapha), which need to be in equilibrium for optimal functioning. Just as Ayurveda seeks to restore balance through personalized treatments, your website should aim to provide tailored solutions or information to meet the unique needs of its visitors. By understanding their requirements and addressing them effectively, your website can promote harmony and fulfillment, much like Ayurvedic principles seek to do for the body and mind. 1' },
        { name: 'Dandruff', image: 'https://res.cloudinary.com/digipumwy/image/upload/v1709719916/ry6cnbvjexaflzpltyi6.png', desc: 'Description 2' },
        { name: 'Snake Venom', image: 'https://res.cloudinary.com/digipumwy/image/upload/v1709719915/ahrn6jasqorsieuxpyak.png', desc: 'Description 3' },
    ];

    const openModal = (medicine) => {
        setSelectedMedicine(medicine);
        setModalOpen(true);
    };

    return (
        <div>
            <div className="pt-1 pb-1" style={{ backgroundColor: '#F4F4F4' }}>
                <center>
                    <small style={{ fontSize: '14px', letterSpacing: '2px' }} className="text-muted text-capitalize">Nirvana Medical Service Hub</small>
                </center>
            </div>
            <Navbar />
            <div className='bg-image'>
                <img src='https://img.freepik.com/free-vector/customer-online-review-rating-feedback-set_124507-8052.jpg?size=626&ext=jpg' className='img-fluid' alt='Sample' />
                <div className='mask' style={{ backgroundColor: '#292929' }}>
                    <div className='d-flex justify-content-center align-items-center h-100'>
                        <p className='text-white h1 mb-0 text-uppercase' style={{ fontSize: '55px', letterSpacing: '2px' }}>Medicine</p>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div className="container">
                <div className="row">
                    {medicines.map((medicine, index) => (
                        <div key={index} className="col-md-4 mb-4">
                            <div className='card' style={{ backgroundColor: "", boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)", minHeight: '350px' }} onClick={() => openModal(medicine)}>
                                <br />
                                <img src={medicine.image} style={{ width: "300px", display: "block", margin: "0 auto" }} className='card-img-top' alt='Medicine' />
                                <div className='card-body'>
                                    <h5 className='card-title'>{medicine.name}</h5>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <Footer />
            <MDBModal show={modalOpen} getOpenState={(isOpen) => setModalOpen(isOpen)} centered>
                <MDBModalBody>
                    {selectedMedicine &&
                        <MDBCard style={{ width: '600px', margin: 'auto' }}>
                            <br />
                            <MDBCardTitle className="text-center" style={{ fontSize: '28px' }}>{selectedMedicine.name}</MDBCardTitle>
                            <img src={selectedMedicine.image} alt={selectedMedicine.name} className='card-img-top' />
                            <MDBCardBody>
                                <MDBCardTitle>{selectedMedicine.name}</MDBCardTitle>
                                <MDBCardText>Description: {selectedMedicine.desc}</MDBCardText>
                            </MDBCardBody>
                            <button
                                style={{
                                    width: "150px",
                                    marginLeft: "420px",
                                    marginBottom: "20px",
                                    backgroundColor: "#007bff",
                                    color: "#fff",
                                    border: "none",
                                    borderRadius: "5px",
                                    padding: "10px",
                                    cursor: "pointer"
                                }}
                                onClick={() => setModalOpen(false)}
                            >
                                Close
                            </button>

                        </MDBCard>
                    }
                </MDBModalBody>
            </MDBModal>
        </div>
    )
};

export default Medicine;
