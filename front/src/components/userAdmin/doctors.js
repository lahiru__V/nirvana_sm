import React, { useState } from 'react';
import Navbar from '../main_parts/navbar.user.log.js';
import Footer from '../main_parts/footer.js';
import Booking from './booking.js';
import { MDBBtn } from 'mdb-react-ui-kit';

function Doctors() {
    const doctors = [
        { name: 'Dr. John', specialty: 'Cardiologist', experience: '10 years', image: 'https://res.cloudinary.com/digipumwy/image/upload/v1709719916/zzo9asi7zzgp5vqoj2qv.png', bio: 'Dr. John is a highly experienced cardiologist with a focus on preventive care.' },
        { name: 'Dr. Emily', specialty: 'Pediatrician', experience: '8 years', image: 'https://res.cloudinary.com/digipumwy/image/upload/v1709719914/cuaigodxtddu8thvymjs.png', bio: 'Dr. Emily specializes in pediatric care, ensuring the health and well-being of children.' },
        { name: 'Dr. Martha', specialty: 'Dermatologist', experience: '12 years', image: 'https://res.cloudinary.com/digipumwy/image/upload/v1709719915/wukubfcsfbhcdw7nbebu.png', bio: 'Dr. Martha is an expert dermatologist offering comprehensive skin care solutions.' },
    ];

    

    const openModal = (doctor) => {
        sessionStorage.setItem("doctor_name", doctor.name);
        window.location.href = "/Booking"; 
    };

    return (
        <div>
            <div className="pt-1 pb-1" style={{ backgroundColor: '#F4F4F4' }}>
                <center>
                    <small style={{ fontSize: '14px', letterSpacing: '2px' }} className="text-muted text-capitalize">Nirvana Medical Service Hub</small>
                </center>
            </div>
            <Navbar />
            <div className='bg-image'>
                <img src='https://img.freepik.com/free-vector/customer-online-review-rating-feedback-set_124507-8052.jpg?size=626&ext=jpg' className='img-fluid' alt='Sample' />
                <div className='mask' style={{ backgroundColor: '#292929' }}>
                    <div className='d-flex justify-content-center align-items-center h-100'>
                        <p className='text-white h1 mb-0 text-uppercase' style={{ fontSize: '55px', letterSpacing: '2px' }}>Doctors</p>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div className="container">
                <div className="row">
                    {doctors.map((doctor, index) => (
                        <div key={index} className="col-md-4 mb-4">
                            <div className='card' style={{ backgroundColor: "", boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)", minHeight: '600px' }}>
                                <img src={doctor.image} style={{ width: "300px", display: "block", margin: "0 auto" }} className='card-img-top' alt='Doctor' />
                                <div className='card-body'>
                                    <h5 className='card-title'>{doctor.name}</h5>
                                    <p className='card-text'>Specialty: {doctor.specialty}</p>
                                    <p className='card-text'>Experience: {doctor.experience}</p>
                                    <p className='card-text'>Bio: {doctor.bio}</p>
                                </div>
                                <MDBBtn style={{ width: "150px", marginLeft: "230px", marginBottom: "20px", }} onClick={() => openModal(doctor)}>Book</MDBBtn>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <Footer />        
        </div>
    );
}

export default Doctors;
